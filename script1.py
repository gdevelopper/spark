"""
Exo 1 : ce fichier représente la solution pour l'éxercice 1
"""

# Basics 
# lecture du fichier README.md de spark et application

# vu qu'on a lancé directement pyspark de spark on n'a pas besoin de creer le Spark context

# Lecture d'un fichier text et création du 1 er RDD
textFile = sc.textFile("spark-3.0.0-preview2-bin-hadoop2.7/README.md")

# nombre de lignes 
textFile.count()

# Affichage de la première ligne
textFile.first()

# appalication d'un filter pour trouver les lignes ayant le Spark
linesWithSpark = textFile.filter(lambda line: "Spark" in line)

# More on RDD Operation 
textFile.map(lambda line : len(line)).reduce(lambda a,b: a if a>b else b) # résultats 22

# Calcul du wordcount avec 
wordCounts = textFile.flatMap(lambda line: line.split()).map(lambda word: (word, 1)).reduceByKey(lambda a, b: a+b)

# question 6 : calculer le top 50 des mots qui apparet le plus dans fichier README.md, top_50 continet des tuple (mot,count) ordonnée par ordre décroissant 
top_50 = wordCounts.sortBy(lambda x : x[1],False).take(50)
