# question 1 :
# Téléchargement du fichier Tiwitter qui ccontient des relatoin folow des utiliseurs 
# le lien de téléchargement https://snap.stanford.edu/data/twitter_combined.txt.gzhttps://snap.stanford.edu/data/twitter_combined.txt.gz

# question 2 :
# à ce niveau je mets le fichier télécharger twitter_combiner.txt.gz dans le path bigdata/data puis je le décompresse avec commande du TP 


# question 3 : cette question consiste à charger les données de twitter dans spark sous forme de RDD
twitter = sc.textFile("data/twitter_combiner.txt")

# question 4 : Observation des entrées du RDD twitter 
# cette commande va me lire la première entrée du rdd
twitter.first()
# output : u'214328887 34428380'

# cette commande va lire 10 entrée du RDD puis les transfomer en un list python 
twitter.take(10) 
# output : [u'214328887 34428380', u'17116707 28465635', u'380580781 18996905', u'221036078 153460275', u'107830991 17868918', u'151338729 222261763', u'19705747 34428380', u'222261763 88323281', u'19933035 149538028', u'158419434 17434613']

# vérification du type des entrée dans le RDD 
type(twitter.first())
# output : <type 'unicode'> 
"""
Pour ce qu concerne l'analyse du type des données lu par spark, on remarque que chaque ligne est vu comme etant une chaine de caractère (string) unicode
Preuve : l'extension u' au début de chaque entrée.
mais aussi pour avoir le coeur net on peut faire 
""" 


# question 5 : nombre d'entrées dans le RDD
twitter.count()
# output : 2 420 766
# ce nombre représente le nombre de relation de folow entre les utilisateurs de twitter 

# question 6 : détérmination du type de chaque entrée du RDD de twitter 
type(twitter.first())
# output : <type 'unicode'>
# ce type n'est pas adapter
# je préférer utilise le type int car c'est des nombre entier séparé par espace 


# question 7 : transformation du type du RDD
twitter  = twitter.map(lambda x : (int(x.split()[0]), int(x.split()[1])))

# question 8 : cette question consiste à compter le nombre de d'utilisateurs twitter  
twitter.flatMap(lambda x : [x[0],x[1]]).distinct().count()
# output : 81306
#la différence entre la méthode Map() et FlatMap() :
#la méthode Map() : Prend un document en entrée et produit un document en sorti
#la méthode FlatMap(): Prend un document en entrée et produit un ou plusieurs documents en sortie.

# question 9 : calcul des degrée entrant de chaque noeud (user)
indegree = twitter.map(lambda x : (x[1],1)).reduceByKey(lambda a,b: a+b)
# pour affiche le contenue 
indegree.take(10)
# output : [(53870594, 20), (19529734, 1), (221904904, 5), (12, 611), (35389442, 1), (14, 10), (81526802, 11), (20, 757), (133824534, 7), (34996248, 15)]


# question 10 : touver les utilisateur les plus connus de twitter ou plus folowe 
most_folowed = indegree.filter(lambda x : x[1] > 1000)
# pour voir leur nombre 
most_folowed.count()
# ouput : 184
# ces sommets la représente les utilisateur les plus suivie dans twitter les start de twitter :p 

